//
//  TRTroupeModel.m
//  TroupeNotifier
//
//  Created by Andrew Newdigate on 23/01/2013.
//  Copyright (c) 2013 Andrew Newdigate. All rights reserved.
//

#import "TRTroupeModel.h"
#import <ISO8601DateFormatter.h>

@implementation TRTroupeModel

+ (TRTroupeModel *) fromDict:(NSDictionary *) dict {
    TRTroupeModel *model = [[TRTroupeModel alloc] init];
    [model updateFromDict:dict];
    return model;
}

- (NSString *)description {
    return [NSString stringWithFormat: @"TRTroupeModel: id=%@, name=%@, lastAccessTime=%@", _id, _name, _lastAccessTime];
}

- (void) updateFromDict:(NSDictionary *) dict {
    
    _id = [dict objectForKey:@"id"];
    _url = [dict objectForKey:@"url"];
    _uri = [dict objectForKey:@"uri"];
    _name = [dict objectForKey:@"name"];
    _githubType = [dict objectForKey:@"githubType"];
    _roomMember = [self toBoolean:[dict objectForKey:@"roomMember"]];
    NSNumber *m = [dict objectForKey:@"mentions"];
    _mentions = m.intValue;
    _lurk = [self toBoolean:[dict objectForKey:@"lurk"]];
    _activity = [self toBoolean:[dict objectForKey:@"activity"]];
    
    id lastAccessString = [dict objectForKey:@"lastAccessTime"];
    if(lastAccessString && [lastAccessString isKindOfClass:[NSString class]]) {
        ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];
        _lastAccessTime = [formatter dateFromString:lastAccessString];
    } else {
        _lastAccessTime = nil;
    }
    
    NSNumber *n = [dict objectForKey:@"unreadItems"];
    _unreadItems = n.intValue;
    
    id o = [dict objectForKey:@"oneToOne"];
    if(o) {
        BOOL troupeOneToOne = [self toBoolean:o];
        
        _oneToOne = troupeOneToOne;
        if(troupeOneToOne) {
            NSDictionary *user = [dict objectForKey:@"user"];
            NSString *userId = [user objectForKey:@"id"];
            _oneToOneUserId = userId;
            _user = user;
        }
    }  else {
        _oneToOne = NO;
    }
    
    id f = [dict objectForKey:@"favourite"];
    if(f) {
        if([f isKindOfClass:[NSNull class]]) {
            _favourite = 0;
        } else if([f isKindOfClass:[NSNumber class]]) {
            _favourite = ((NSNumber *) f).intValue;
        } else {
            BOOL favourite = [self toBoolean:f];
            _favourite = favourite ? 1000 : 0;
        }
    } else {
        _favourite = 0;
    }
    

}

- (void)patchFromDict:(id)dict {
    NSSet *keys = [NSSet setWithArray:[dict allKeys]];

    if([keys containsObject:@"url"]) _url = [dict objectForKey:@"url"];
    if([keys containsObject:@"uri"]) _uri = [dict objectForKey:@"uri"];
    if([keys containsObject:@"name"]) _name = [dict objectForKey:@"name"];
    if([keys containsObject:@"githubType"]) _githubType = [dict objectForKey:@"githubType"];
    if([keys containsObject:@"roomMember"]) _roomMember = [self toBoolean:[dict objectForKey:@"roomMember"]];
    if([keys containsObject:@"mentions"]) {
        NSNumber *n = [dict objectForKey:@"mentions"];
        _mentions = n.intValue;
    }
    if([keys containsObject:@"lurk"]) _lurk = [self toBoolean:[dict objectForKey:@"lurk"]];
    if([keys containsObject:@"activity"]) _activity = [self toBoolean:[dict objectForKey:@"activity"]];
    if([keys containsObject:@"lastAccessTime"]) {
        id lastAccessString = [dict objectForKey:@"lastAccessTime"];
        if(lastAccessString && [lastAccessString isKindOfClass:[NSString class]]) {
            ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];
            _lastAccessTime = [formatter dateFromString:lastAccessString];
        } else {
            _lastAccessTime = nil;
        }
    }

    if([keys containsObject:@"unreadItems"]) {
        NSNumber *n = [dict objectForKey:@"unreadItems"];
        _unreadItems = n.intValue;
    }

    id f = [dict objectForKey:@"favourite"];
    if(f) {
        if([f isKindOfClass:[NSNull class]]) {
            _favourite = 0;
        } else if([f isKindOfClass:[NSNumber class]]) {
            _favourite = ((NSNumber *) f).intValue;
        } else {
            BOOL favourite = [self toBoolean:f];
            _favourite = favourite ? 1000 : 0;
        }
    }

    // You can't patch oneToOne, it'll never change
}

- (BOOL)toBoolean:(id)anObject {
    if (anObject) {
        CFBooleanRef cfBoolean = CFBridgingRetain(anObject);
        BOOL boolean = CFBooleanGetValue(cfBoolean);
        CFRelease(cfBoolean);
        return boolean;
    } else {
        return NO;
    }
}

@end
