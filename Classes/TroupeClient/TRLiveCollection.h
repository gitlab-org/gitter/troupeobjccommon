//
//  TRLiveCollection.h
//  TroupeNotifier
//
//  Created by Andrew Newdigate on 20/08/2013.
//  Copyright (c) 2013 Andrew Newdigate. All rights reserved.
//

#import <Foundation/Foundation.h>


@class TRLiveCollection;

@protocol ModelWithId
@property (nonatomic, readonly) NSString* id;
@end


@protocol TRLiveCollectionDelegate
- (id) deserialize:(NSDictionary *)dict;
- (void) update:(id) existingModel with:(NSDictionary *) dict;
- (void) patch:(id) existingModel with:(NSDictionary *) dict collection:(NSArray *)collection;
- (void) remove:(NSString *) idString withModal:(NSDictionary *)existingModel;
- (void) sort:(NSArray *) array;
@end


@protocol TRLiveCollectionChangeDelegate
@optional
- (void) collectionDidChange:(TRLiveCollection *) collection;
@end

@interface TRLiveCollection : NSObject

@property (nonatomic, readonly) NSArray* items;
- (id)initWithDelegate:(id<TRLiveCollectionDelegate>) delegate withChangeDelegate:(id<TRLiveCollectionChangeDelegate>) changeDelegate;
- (void) handleSnapshot:(NSArray *) snapshot;
- (void) handleMessage:(NSDictionary *) message;
- (id)findById:(NSString *)idString;
- (void) removeAllObjects;
@end
