//
//  TRLiveCollection.m
//  TroupeNotifier
//
//  Created by Andrew Newdigate on 20/08/2013.
//  Copyright (c) 2013 Andrew Newdigate. All rights reserved.
//

#import "TRLiveCollection.h"

@interface TRLiveCollection ()
- (void) handleUpdateOperation:(NSDictionary *)model;
- (void) handleCreateOperation:(NSDictionary *)model;
- (void) handleRemoveOperation:(NSDictionary *)model;
- (void) handlePatchOperation:(NSDictionary *)model;
@end

@implementation TRLiveCollection {
    id<TRLiveCollectionDelegate> _delegate;
    id<TRLiveCollectionChangeDelegate> _changeDelegate;
    NSMutableArray* _items;
}

- (id)initWithDelegate:(id<TRLiveCollectionDelegate>) delegate withChangeDelegate:(id<TRLiveCollectionChangeDelegate>) changeDelegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _changeDelegate = changeDelegate;
        _items = [NSMutableArray arrayWithCapacity:0];
    }
    return self;
}

- (void) handleSnapshot:(NSArray *) snapshot {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:snapshot.count];
    [snapshot enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = obj;
        
        [result addObject:[_delegate deserialize:dict]];
    }];

    [_delegate sort:_items];
    
    [_items removeAllObjects];
    [_items addObjectsFromArray:result];
    
    [_changeDelegate collectionDidChange:self];
}

- (void) handleMessage:(NSDictionary *) message {
    NSString *operation = [message objectForKey:@"operation"];
    NSDictionary *model = [message objectForKey:@"model"];
    if(operation == nil || model == nil) return;
    
    if([operation isEqualToString:@"update"]) {
        [self handleUpdateOperation:model];
    } else if([operation isEqualToString:@"create"]) {
        [self handleCreateOperation:model];
    } else if([operation isEqualToString:@"remove"]) {
        [self handleRemoveOperation:model];
    } else if([operation isEqualToString:@"patch"]) {
        [self handlePatchOperation:model];
    } else {
        qlinfo(@"Unknown operation: %@, ignoring", operation);
    }

}

- (NSArray *)items {
    return _items;
}

- (void)removeAllObjects {
    [_items removeAllObjects];
    [_changeDelegate collectionDidChange:self];
}

- (void) handleUpdateOperation:(NSDictionary *)model {
    NSString *idString = [model objectForKey:@"id"];
    
    id existingModel = [self findById:idString];
    if(!existingModel) {
        [self handleCreateOperation:model];
        return;
    }
    
    [_delegate update:existingModel with:model];
    [_changeDelegate collectionDidChange:self];
}

- (void) handleCreateOperation:(NSDictionary *)model {
    NSString *modelId = [model objectForKey:@"id"];
    id existingItem = [self findById:modelId];
    if(existingItem) {
        [self handleUpdateOperation:model];
        return;
    }
    
    existingItem = [_delegate deserialize:model];
    [_items addObject:existingItem];
    
    [_changeDelegate collectionDidChange:self];
}

- (void) handleRemoveOperation:(NSDictionary *)model {
    NSString *idString = [model objectForKey:@"id"];
    id existingModel = [self findById:idString];
    
    [_delegate remove:idString withModal:existingModel];
    
    if(!existingModel) {
        return;
    }
    

    [_items removeObject:existingModel];
    [_changeDelegate collectionDidChange:self];
}

- (void) handlePatchOperation:(NSDictionary *)model {
    NSString *idString = [model objectForKey:@"id"];
    id existingModel = [self findById:idString];

    if(!existingModel) {
        return;
    }
    
    [_delegate patch:existingModel with:model collection:self.items];
    [_changeDelegate collectionDidChange:self];
}


- (id)findById:(NSString *)idString {
    for(id<ModelWithId> model in _items) {
        if([model.id isEqualToString:idString]) {
            return model;
        }
    }
    
    return nil;
}

@end
