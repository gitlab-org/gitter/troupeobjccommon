//
//  TRTroupeList.m
//  Troupe
//
//  Created by Andrew Newdigate on 01/04/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import "TRTroupeList.h"
#import "TRAuthController.h"
#import "TRNotifications.h"
#import "TRDefines.h"

static TRTroupeList *sharedSingleton;

@interface TRLiveTroupeCollectionDelegate: NSObject <TRLiveCollectionDelegate>
@end

//#if !GITTER
//@interface TRLiveInviteCollectionDelegate: NSObject <TRLiveCollectionDelegate>
//@end
//#endif

@interface TRTroupeList ()

- (void) startServerSubscription;

- (void) stopServerSubscription;

- (void) handleTroupeUnreadNotification:(NSDictionary *)dict;

- (void) handleTroupeMentionNotification:(NSDictionary *)dict;

- (void) countUnreadItems;

@end

@implementation TRLiveTroupeCollectionDelegate {
}

- (id)init
{
    self = [super init];
    return self;
}


- (id) deserialize:(NSDictionary *)dict {
    return [TRTroupeModel fromDict:dict];
}

/* When a favourite is updated, we need to reorder the rest of the favourites in response */
- (void) reorderFavourites:(TRTroupeModel *) model  collection:(NSArray *)collection {
    NSArray *candidates = [collection filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        TRTroupeModel *e = evaluatedObject;
        if(e.favourite <= 0) return NO;
        if(e == model) return NO;
        return e.favourite >= model.favourite;
    }]];
    
    candidates = [candidates sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        TRTroupeModel *t1 = obj1;
        TRTroupeModel *t2 = obj2;
        
        return (NSComparisonResult) t1.favourite - t2.favourite;
    }];
    
    int next = model.favourite;
    
    for(TRTroupeModel *item in candidates) {
        if(item.favourite > next) {
            break;
        }
        
        item.favourite++;
        next = item.favourite;
    }
    
}

- (void) update:(id) existingModel with:(NSDictionary *) dict {
    TRTroupeModel *model = existingModel;
    [model updateFromDict:dict];
}

- (void) patch:(id) existingModel with:(NSDictionary *) dict collection:(NSArray *)collection {
    TRTroupeModel *model = existingModel;
    [model patchFromDict:dict];
    
    id f = [dict objectForKey:@"favourite"];
    if(f && model.favourite > 0) {
        /* Favourite was updated, we need to reorder the rest of the favourites too */
        [self reorderFavourites:model collection:collection];
    }
}

- (void) remove:(NSString *) idString withModal:(NSDictionary *)existingModel {
}

- (void) sort:(NSMutableArray *) array {
    
    [array sortUsingComparator:^(id a, id b) {
        TRTroupeModel *t1 = a;
        TRTroupeModel *t2 = b;
        
        return [t1.uri caseInsensitiveCompare:t2.uri];
    }];
    
}

@end

@implementation TRTroupeList {
    TRLiveCollection *_troupes;
    TREventController *_eventController;

    //NSInteger _unreadTroupeCount;
    
    NSPredicate *_allConversationsPredicate;
    NSPredicate *_favouritePredicate;
    NSPredicate *_unreadPredicate;
    NSPredicate *_recentPredicate;
    NSPredicate *_oneToOnePredicate;
    NSPredicate *_orgPredicate;
    NSPredicate *_repoPredicate;
    
#if TARGET_OS_IPHONE
#else
    BOOL _screenSaverRunning;
    BOOL _screenLocked;
#endif
}

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[TRTroupeList alloc] init];
    }
}


+ (TRTroupeList *) sharedInstance
{
    return sharedSingleton;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        _allConversationsPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return t1.favourite > 0 || t1.mentions > 0 || t1.unreadItems > 0 || t1.lastAccessTime != nil;
        }];

        _troupes = [[TRLiveCollection alloc] initWithDelegate:[[TRLiveTroupeCollectionDelegate alloc] init] withChangeDelegate:self];

        _favouritePredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return t1.favourite > 0;
        }];
        
        _unreadPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return t1.unreadItems > 0;
        }];
        
        _recentPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return t1.lastAccessTime != nil  && t1.favourite <= 0;
        }];

        _oneToOnePredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return t1.oneToOne;
        }];

        _orgPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return [t1.githubType isEqualToString:@"ORG"];
        }];
        
        _repoPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            TRTroupeModel *t1 = evaluatedObject;
            return ![t1.githubType isEqualToString:@"ORG"] && !t1.oneToOne;
        }];
        
        _unreadTroupeCount = 0;
    }
    return self;
}

- (NSArray *)favouriteTroupes {
    NSArray *favs = [_troupes.items filteredArrayUsingPredicate:_favouritePredicate];
    
    NSArray *sorted = [favs sortedArrayUsingComparator:^(id obj1, id obj2) {
        TRTroupeModel *t1 = obj1;
        TRTroupeModel *t2 = obj2;
        
        int f1 = t1.favourite;
        int f2 = t2.favourite;
        
        if(f1 != 0) {
            if(f2 != 0) {
                int c = f1 - f2;
                if(c == 0) {
                    return [t1.name.lowercaseString compare:t2.name.lowercaseString];
                }
                
                return (NSComparisonResult) c;
            } else {
                return NSOrderedAscending;
            }
        } else {
            if(f2 != 0) {
                return NSOrderedDescending;
            } else {
                return [t1.name.lowercaseString compare:t2.name.lowercaseString];
            }
        }
    }];
    
    return sorted;
}

- (NSArray *)oneToOneTroupes {
    NSArray *list = [_troupes.items filteredArrayUsingPredicate:_oneToOnePredicate];
    return [self sortTroupeListByName:list];
}

- (NSArray *)orgRooms {
    return [_troupes.items filteredArrayUsingPredicate:_orgPredicate];
}

- (NSArray *)repoRooms {
    NSArray *list = [_troupes.items filteredArrayUsingPredicate:_repoPredicate];
    return [self sortTroupeListByName:list];
}

- (NSArray *)sortTroupeListByName:(NSArray *)array {
    return [array sortedArrayUsingComparator:^(id obj1, id obj2) {
        TRTroupeModel *t1 = obj1;
        TRTroupeModel *t2 = obj2;

        NSString *d1 = [t1.name lowercaseString];
        NSString *d2 = [t2.name lowercaseString];

        return [d1 compare:d2];
    }];
}

- (NSArray *)unreadTroupes {
    return [_troupes.items filteredArrayUsingPredicate:_unreadPredicate];
}

- (NSArray *)recentTroupes {
    NSArray *recent = [_troupes.items filteredArrayUsingPredicate:_recentPredicate];
    NSArray *sorted = [recent sortedArrayUsingComparator:^(id obj1, id obj2) {
        TRTroupeModel *t1 = obj1;
        TRTroupeModel *t2 = obj2;

        NSDate *d1 = t1.lastAccessTime;
        NSDate *d2 = t2.lastAccessTime;

        /* NB: Switch this list around (compare d2 to d1, not visa-versa) */
        return [d2 compare:d1];
    }];

    if(sorted.count <= 5) {
        return sorted;
    } else {
        return [sorted subarrayWithRange:NSMakeRange(0, 5)];
    }

}


NSComparator compareNames = ^(id obj1, id obj2) {
    TRTroupeModel *t1 = obj1;
    TRTroupeModel *t2 = obj2;

    NSString *d1 = [t1.name lowercaseString];
    NSString *d2 = [t2.name lowercaseString];

    return [d1 compare:d2];
};

/**
 * Assumes t1 or t2 are favourites.
 * Given 3,2,0,1
 * Orders troupes in the following way:
 * 1,2,3,0
 */
NSComparator compareFavourites = ^(id obj1, id obj2) {
    TRTroupeModel *t1 = obj1;
    TRTroupeModel *t2 = obj2;
    if(t1.favourite == 0) {
        return NSOrderedDescending;
    } else if(t2.favourite == 0) {
        return NSOrderedAscending;
    } else if(t1.favourite < t2.favourite) {
        return NSOrderedAscending;
    } else if (t2.favourite < t1.favourite) {
        return NSOrderedDescending;
    } else {
        // both are favourites
        return compareNames(t1, t2);
    }
};

/**
 * Assumes t1 or t2 have unread messages.
 */
NSComparator compareUnread = ^(id obj1, id obj2) {
    TRTroupeModel *t1 = obj1;
    TRTroupeModel *t2 = obj2;

    if(t1.unreadItems > 0) {
        return NSOrderedAscending;
    } else if (t2.unreadItems > 0) {
        return NSOrderedDescending;
    } else {
        // both have unread messages
        return compareNames(t1, t2);
    }
};

/**
 * Assumes t1 or t2 have mentions.
 */
NSComparator compareMentions = ^(id obj1, id obj2) {
    TRTroupeModel *t1 = obj1;
    TRTroupeModel *t2 = obj2;

    if(t1.mentions > 0) {
        return NSOrderedAscending;
    } else if (t2.mentions > 0) {
        return NSOrderedDescending;
    } else {
        // both have mentions
        return compareNames(t1, t2);
    }
};

NSComparator compareLastAccessTime = ^(id obj1, id obj2) {
    TRTroupeModel *t1 = obj1;
    TRTroupeModel *t2 = obj2;
    NSDate *d1 = t1.lastAccessTime;
    NSDate *d2 = t2.lastAccessTime;

    if ([d1 compare:d2] == NSOrderedDescending) {
        return NSOrderedAscending;
    } else if ([d1 compare:d2] == NSOrderedAscending) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
};

NSComparator compareTroupes = ^(id obj1, id obj2) {
    TRTroupeModel *t1 = obj1;
    TRTroupeModel *t2 = obj2;

    NSComparisonResult result;
    if(t1.favourite > 0 || t2.favourite > 0) {
        result = compareFavourites(t1, t2);
    } else if (t1.mentions > 0 || t2.mentions > 0) {
        result = compareMentions(t1, t2);
    } else if(t1.unreadItems > 0 || t2.unreadItems > 0) {
        result = compareUnread(t1, t2);
    } else {
        result = compareLastAccessTime(t1, t2);
    }

    return result;
};

- (NSArray *)allConversations {
    NSArray *recent = [_troupes.items filteredArrayUsingPredicate:_allConversationsPredicate];
    NSArray *sorted = [recent sortedArrayUsingComparator:compareTroupes];
    return sorted;
}

- (void) startListening {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(refresh:)
                   name:TroupeUrlChanged
                 object:nil];
    
    [center addObserver:self
               selector:@selector(signedOut:)
                   name:TroupeSignedOut
                 object:nil];
    
    
    [center addObserver:self
               selector:@selector(refresh:)
                   name:TroupeAuthenticationReady
                 object:nil];
    
#if TARGET_OS_IPHONE

    [center addObserver:self
               selector:@selector(applicationDidEnterBackground:)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];

    [center addObserver:self
               selector:@selector(applicationWillEnterForeground:)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];  
    
    [center addObserver:self
               selector:@selector(applicationWillTerminate:)
                   name:UIApplicationWillTerminateNotification
                 object:nil];

    // Fetch from the cache
#else

    NSDistributedNotificationCenter *distributeNotifications = [NSDistributedNotificationCenter defaultCenter];
    
    [distributeNotifications addObserver:self
                                selector:@selector(screenIsLocked:)
                                    name:@"com.apple.screenIsLocked"
                                  object:nil];
    
    [distributeNotifications addObserver:self
                                selector:@selector(screenIsUnlocked:)
                                    name:@"com.apple.screenIsUnlocked"
                                  object:nil];
    
    [distributeNotifications addObserver:self
                                selector:@selector(screenSaverDidStart:)
                                    name:@"com.apple.screensaver.didstart"
                                  object:nil];
    
    [distributeNotifications addObserver:self
                                selector:@selector(screenSaverDidStop:)
                                    name:@"com.apple.screensaver.didstop"
                                  object:nil];
    
#endif
    
    [self startServerSubscription];
    
}

- (void) stopListening {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];

    [center removeObserver:self];
    
#if !TARGET_OS_IPHONE
    NSDistributedNotificationCenter *distributeNotifications = [NSDistributedNotificationCenter defaultCenter];

    [distributeNotifications removeObserver:self];
#endif
    
}

- (void) startServerSubscription {
    [self stopServerSubscription];
    
    if(![[TRAuthController sharedInstance] authenticated]) {
        return;
    }
    
    qldebug(@"Event bridge is attempting to login");
    
    _eventController = [[TREventController alloc] initWithDelegate:self];
}

- (void) stopServerSubscription {
    if(_eventController == nil) {
        return;
    }
    
    [_eventController disconnect];

    _eventController = nil;
}

// Delegate method
- (void) serverSubscriptionDisconnected {
    qlinfo(@"Server subscription disconnected");
}

- (void) serverSubscriptionEstablished {
    // No need to do this any longer as we get a snapshot: [self refreshTroupes];

}

- (void) snapshotReceived:(id) snapshot channel:(NSString *)channel {
    qldebug(@"Snapshot received on channel %@", channel);
    if([channel hasSuffix:TROUPES_SUBCHANNEL]) {
        NSArray *troupes = snapshot;
        
        [_troupes handleSnapshot:troupes];
    }
}

- (void) signedOut:(NSNotification *)notification {
    [_troupes removeAllObjects];
    [self refresh:notification];
}

- (void) refresh:(NSNotification *)notification {
    [self stopServerSubscription];
    [self startServerSubscription];
}

#if TARGET_OS_IPHONE
- (void) applicationDidEnterBackground:(NSNotification *)notification {
    [self stopServerSubscription];
}

- (void) applicationWillEnterForeground:(NSNotification *)notification {
    [self stopServerSubscription];
    [self startServerSubscription];
}

- (void) applicationWillTerminate:(NSNotification *)notification {
    [self stopServerSubscription];
}
#endif

- (void) messageReceived:(NSDictionary *)messageDict channel:(NSString *)channel {
    
    if([channel hasSuffix:TROUPES_SUBCHANNEL]) {
        [_troupes handleMessage:messageDict];
    } else {
        NSString *n = [messageDict objectForKey:@"notification"];
        if(n == nil) return;
        
        if([n isEqualToString:@"troupe_unread"]) {
            [self handleTroupeUnreadNotification:messageDict];
        }

        if([n isEqualToString:@"troupe_mention"]) {
            [self handleTroupeMentionNotification:messageDict];
        }
    }
}


- (void) handleTroupeUnreadNotification:(NSDictionary *)dict {
    NSString *troupeId = [dict objectForKey:@"troupeId"];
    NSNumber *totalUnreadItems = [dict objectForKey:@"totalUnreadItems"];
    
    BOOL found = false;
    for(TRTroupeModel *troupe in _troupes.items) {
        NSString *tId = troupe.id;
        if([tId isEqualToString:troupeId]) {
            troupe.unreadItems = totalUnreadItems.intValue;
            found = true;
        }
    }
    
    [self countUnreadItems];

    if(found) {
        [[NSNotificationCenter defaultCenter] postNotificationName:TroupeListChanged object:nil];
    } else {
        qlwarning(@"Received a notification for an unknown troupe. Something fishy is happening. Refreshing troupes to be sure.");
        [_eventController reconnect];
    }
}

- (void) handleTroupeMentionNotification:(NSDictionary *)dict {
    NSString *troupeId = [dict objectForKey:@"troupeId"];
    NSNumber *mentions = [dict objectForKey:@"mentions"];

    BOOL found = false;
    for(TRTroupeModel *troupe in _troupes.items) {
        NSString *tId = troupe.id;
        if([tId isEqualToString:troupeId]) {
            troupe.mentions = mentions.intValue;
            found = true;
        }
    }

    [self countMentions];
    
    if(found) {
        [[NSNotificationCenter defaultCenter] postNotificationName:TroupeListChanged object:nil];
    } else {
        qlwarning(@"Received a notification for an unknown troupe. Something fishy is happening. Refreshing troupes to be sure.");
        [_eventController reconnect];
    }
}

#if TARGET_OS_IPHONE
- (void)forceRefresh {
    [_eventController reconnect];
}
#endif

#if !TARGET_OS_IPHONE
- (void) screenIsLocked:(NSNotification *)notification {
    qldebug(@"Screen locked");
    
    _screenLocked = YES;
    if(_screenLocked || _screenSaverRunning) {
        [self stopServerSubscription];
    }
}

- (void) screenIsUnlocked:(NSNotification *)notification {
    qldebug(@"Screen unlocked");

    _screenLocked = NO;
    _screenSaverRunning = NO; // We don't seem to get a separate event for this...
    if(!_screenLocked && !_screenSaverRunning) {
        [self stopServerSubscription];
        [self startServerSubscription];
    }
}

- (void) screenSaverDidStart:(NSNotification *)notification {
    qldebug(@"Screensaver started");

    _screenSaverRunning = YES;
    if(_screenLocked || _screenSaverRunning) {
        [self stopServerSubscription];
    }
}

- (void) screenSaverDidStop:(NSNotification *)notification {
    qldebug(@"Screensaver stopped");

    _screenSaverRunning = NO;
    if(!_screenLocked && !_screenSaverRunning) {
        [self stopServerSubscription];
        [self startServerSubscription];
    }
}

#endif

- (void) countUnreadItems {
    int unreadTroupeCount = 0;
    
    for(TRTroupeModel *troupe in _troupes.items) {
        if(troupe.unreadItems > 0) {
            unreadTroupeCount++;
        }
    }
    
    _unreadTroupeCount = unreadTroupeCount;
    //_unreadTroupeCount = unreadTroupeCount;
    
    NSNumber *v = [NSNumber numberWithInteger:unreadTroupeCount];
    // Post about unread troupes...
    [[NSNotificationCenter defaultCenter] postNotificationName:TroupeUserUnreadCountsChanged object:v];
#if TARGET_OS_IPHONE
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:unreadTroupeCount];
#else
    NSDockTile *tile = [[NSApplication sharedApplication] dockTile];

    if(unreadTroupeCount > 0) {
        [tile setBadgeLabel:[@(unreadTroupeCount) description]];
        [tile setShowsApplicationBadge:YES];
    } else {
        [tile setBadgeLabel:@""];
        [tile setShowsApplicationBadge:NO];
    }
#endif
}

- (void) countMentions {
    int mentionedTroupeCount = 0;

    for(TRTroupeModel *troupe in _troupes.items) {
        if(troupe.mentions > 0) {
            mentionedTroupeCount++;
        }
    }

    _mentionedTroupeCount = mentionedTroupeCount;

    NSNumber *v = [NSNumber numberWithInteger:mentionedTroupeCount];
    [[NSNotificationCenter defaultCenter] postNotificationName:TroupeUserMentionCountsChanged object:v];
}

- (void) collectionDidChange:(TRLiveCollection *)collection {
    [self countUnreadItems];
    [self countMentions];
    [[NSNotificationCenter defaultCenter] postNotificationName:TroupeListChanged object:nil];
}

- (BOOL) connectionEstablished {
    return _eventController && _eventController.subscriptionReady;
}

- (NSArray *)filter:(NSString *)searchText {
    NSPredicate *p = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        TRTroupeModel *t1 = evaluatedObject;
        if([t1.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
            return YES;
        }
        
        return NO;
    }];
    
    NSArray *list = [_troupes.items filteredArrayUsingPredicate:p];
    return [self sortTroupeListByName:list];
}

- (TRTroupeModel *) lastAccessedTroupe {
    NSDate *max = nil;
    TRTroupeModel *candidate = nil;
    for (TRTroupeModel *t in _troupes.items) {
        NSDate *d = t.lastAccessTime;
        if(max && d) {
            if([d compare:max] == NSOrderedDescending) {
                max = d;
                candidate = t;
            }
        } else if(d) {
            max = d;
            candidate = t;
        }
    }
    
    return candidate;
}

@end
