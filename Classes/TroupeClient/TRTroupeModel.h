//
//  TRTroupeModel.h
//  TroupeNotifier
//
//  Created by Andrew Newdigate on 23/01/2013.
//  Copyright (c) 2013 Andrew Newdigate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRTroupeModel : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *uri;
@property (nonatomic) int favourite;
@property (nonatomic) int unreadItems;
@property (nonatomic) NSDate *lastAccessTime;
@property (nonatomic, strong) NSString *oneToOneUserId;
@property (nonatomic) NSString *githubType;
@property (nonatomic) BOOL roomMember;
@property (nonatomic) int mentions;
@property (nonatomic) BOOL lurk;
@property (nonatomic) BOOL activity;
@property (nonatomic) NSDictionary *user;

@property (nonatomic) BOOL oneToOne;

- (void) updateFromDict:(NSDictionary *) dict;
+ (TRTroupeModel *) fromDict:(NSDictionary *) dict;

- (void)patchFromDict:(id)model;

@end
