//
//  TRNotifications.m
//  Troupe
//
//  Created by Andrew Newdigate on 01/04/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import "TRNotifications.h"   

NSString *const TroupeUrlChanged = @"TroupeUrlChanged";
NSString *const TroupeNotificationAccessGranted = @"TroupeNotificationAccessGranted";
NSString *const TroupeLocationAccessGranted = @"TroupeLocationAccessGranted";
NSString *const TroupeAuthenticationReady = @"TroupeAuthenticationReady";
NSString *const TroupeSignInSuccessful = @"TroupeSignInSuccessful";

NSString *const TroupeSignedOut = @"TroupeSignedOut";
NSString *const TroupeSignOutRequested = @"TroupeSignOutRequested";
NSString *const TroupeContextDidChange = @"TroupeContextDidChange";
NSString *const TroupeContextChanged = @"TroupeContextChanged";
NSString *const IncomingUserNotification = @"IncomingUserNotification";
NSString *const IncomingTroupeNotification = @"IncomingTroupeNotification";
NSString *const UserIdChanged = @"UserIdChanged";

NSString *const TroupeRealtimeActivated = @"TroupeRealtimeActivated";
NSString *const TroupeListChanged = @"TroupeListChanged";
NSString *const TroupeListError = @"TroupeListError";

NSString *const TroupeTitleChange = @"TroupeTitleChange";

NSString *const TroupeUserUnreadCountsChanged = @"TroupeUserUnreadCountsChanged";
NSString *const TroupeUserMentionCountsChanged = @"TroupeUserMentionCountsChanged";


NSString *const TroupeRequireOAuth = @"TroupeRequireOAuth";
NSString *const TroupeCompletedOAuth = @"TroupeCompletedOAuth";

NSString *const AuthTokenRevoked = @"AuthTokenRevoked";
