//
//  TRNotificationsManager.m
//  Troupe
//
//  Created by Andrew Newdigate on 21/11/2012.
//
//

#import "TRNotificationsManager.h"
#import "GTMHTTPFetcher.h"
#import "TRAppSettings.h"
#import "NSData+Base64.h"
#import "NSObject+URLEncoding.h"
#import "TRAuthController.h"
#import "TRAppSettings.h"
#import "TRNotifications.h"

@interface TRNotificationsManager ()

- (void) accessGranted:(NSNotification *)notification;

- (void) authenticated;

@end

static TRNotificationsManager *sharedNotificationsManager;

@implementation TRNotificationsManager

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedNotificationsManager = [[TRNotificationsManager alloc] init];
    }
}

+ (TRNotificationsManager *) sharedInstance
{
    return sharedNotificationsManager;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void) start {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];

    [center addObserver:self
               selector:@selector(troupeSignedOut:)
                   name:TroupeSignedOut
                 object:nil];
    
    
    [center addObserver:self
               selector:@selector(troupeAuthenticationReady:)
                   name:TroupeAuthenticationReady
                 object:nil];
    
    if([[TRAuthController sharedInstance] preAuthenticatedAuthenticator]) {
        [self authenticated];
    }
    
    if([[TRAppSettings sharedInstance] notificationAccessGranted]) {
        [self accessGranted:nil];
    } else {
        [center addObserver:self
                   selector:@selector(accessGranted:)
                       name:TroupeNotificationAccessGranted
                     object:nil];
    }
}

- (void)accessGranted:(NSNotification *)notification {

    UIMutableUserNotificationAction *action = [[UIMutableUserNotificationAction alloc] init];
    [action setIdentifier: @"CHAT_REPLY"];
    [action setTitle:@"Reply"];
    [action setActivationMode:UIUserNotificationActivationModeBackground];
    [action setDestructive:NO];
    [action setAuthenticationRequired:NO];
    [action setBehavior:UIUserNotificationActionBehaviorTextInput];

    UIMutableUserNotificationCategory *category = [[UIMutableUserNotificationCategory alloc] init];
    [category setIdentifier:@"NEW_CHAT"];
    [category setActions:@[action] forContext:UIUserNotificationActionContextDefault];
    [category setActions:@[action] forContext:UIUserNotificationActionContextMinimal];

    UIApplication *application = [UIApplication sharedApplication];
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)
                                                                                    categories:[NSSet setWithObject:category]]];
    [application registerForRemoteNotifications];
}

- (void) troupeSignedOut:(NSNotification *)notification {
    //[self deregisterDeviceToUser:uniqueDeviceId];
}

- (void) troupeAuthenticationReady:(NSNotification *)notification {
    [self authenticated];
    
    if([[TRAppSettings sharedInstance] notificationAccessGranted]) {
        [self accessGranted:nil];
    }
}

- (void) authenticated {
    NSString * uniqueDeviceId = [[TRAppSettings sharedInstance] uniqueDeviceId];
    [self registerDeviceToUser:uniqueDeviceId
              forAuthorization:[[TRAuthController sharedInstance] preAuthenticatedAuthenticator]];
}

- (void) registerDeviceForNotifications:(NSData*) data forUniqueDeviceId:(NSString *) deviceId {
    qlinfo(@"registerDeviceForNotifications: %@", deviceId);
    
    NSString *deviceName = [[TRAppSettings sharedInstance] deviceName];
    
    NSString *encoded = [data base64EncodedString];
#ifdef BETA
    #ifdef RELEASE
        NSString *deviceType = @"APPLE-BETA";
    #else
        NSString *deviceType = @"APPLE-BETA-DEV";
    #endif

#else
    #ifdef RELEASE
        NSString *deviceType = @"APPLE";
    #else
        NSString *deviceType = @"APPLE-DEV";
    #endif

#endif
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"];
    NSString* buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    
    NSString *body = [NSString stringWithFormat:@"deviceId=%@&deviceType=%@&deviceToken=%@&deviceName=%@&version=%@&build=%@",
                                                    [deviceId URLEncodedString],
                                                    [deviceType URLEncodedString],
                                                    [encoded URLEncodedString],
                                                    [deviceName URLEncodedString],
                                                    [appVersion URLEncodedString],
                                                    [buildNumber URLEncodedString]];
    
    qlinfo(@"registerDeviceForNotifications: body=%@", body);
    
    
    NSData *requestData = [ NSData dataWithBytes: [body UTF8String] length: body.length ];
    
    NSURL *apiUrl = [[TRAppSettings sharedInstance] baseAPIURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[apiUrl URLByAppendingPathComponent:@"/v1/apn"]];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = requestData;
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    GTMHTTPFetcher* fetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    fetcher.retryEnabled = YES;
    fetcher.maxRetryInterval = 60.0;
    [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
        if (error != nil) {
            // status code or network error
            qlerror(@"Error occurred while communicating with server %@", error);
        } else {
            // succeeded
            qlinfo(@"Registration completed successfully");
        }
    }];

}

- (void) registerDeviceToUser:(NSString *) deviceId forAuthorization:(GTMOAuth2Authentication *) auth {
    qlinfo(@"registerDeviceToUser: %@", deviceId);
    
    NSString *body = [NSString stringWithFormat:@"deviceId=%@", deviceId];
    
    NSData *requestData = [ NSData dataWithBytes: [body UTF8String] length: body.length ];
    
    NSURL *apiUrl = [[TRAppSettings sharedInstance] baseAPIURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[apiUrl URLByAppendingPathComponent:@"/v1/userapn"]];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = requestData;
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [auth authorizeRequest:request
              completionHandler:^(NSError *error) {
                  if (error) {
                      qlerror(@"Error authorizing request: %@", error);
                  } else {
                      GTMHTTPFetcher* fetcher = [GTMHTTPFetcher fetcherWithRequest:request];
                      fetcher.retryEnabled = YES;
                      fetcher.maxRetryInterval = 60.0;
                      [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *error) {
                          if (error != nil) {
                              // status code or network error
                              qlerror(@"Error occurred while communicating with server %@", error);
                          } else {
                              // succeeded
                              qlinfo(@"User registration completed successfully");
                          }
                      }];
                  }
              }];
    

}

@end
