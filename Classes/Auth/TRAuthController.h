//
//  AuthController.h
//  OAuth2SampleTouch
//
//  Created by Andrew Newdigate on 11/11/2012.
//
//

#import <Foundation/Foundation.h>
#import "GTMOAuth2SignIn.h"

// Auth Delegate

@interface TRAuthController : NSObject 

+ (TRAuthController *) sharedInstance;

- (void)signOut;

- (BOOL) authenticated;

- (GTMOAuth2Authentication*) authenticator;

- (GTMOAuth2Authentication*) preAuthenticatedAuthenticator;

- (NSString *) authToken;


#if TARGET_OS_IPHONE

- (void)authenticate:(void (^)(UIViewController *authWindow))presentAuthWindow
           completed:(void (^)(BOOL success))completed;

- (void) intercept:(id)presentView;
- (void) endIntercept;

#else
- (void)authenticate:(void (^)(BOOL success))completed;

#endif

@end
