
#import "TRAuthController.h"
#import "GTMOAuth2SignIn.h"
#import "TRAppSettings.h"
#import "TRNotifications.h"

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>

#import "GTMOAuth2ViewControllerTouch.h"
#else
#import "GTMOAuth2WindowController.h"
#endif

// Only ever do this once
static BOOL *cacheCleared = NO;

// ----------------------------------------------------
// Auth controller private inteface
@interface TRAuthController ()
#if TARGET_OS_IPHONE
<UIWebViewDelegate>
#else
#endif

@property (nonatomic) BOOL authenticationReady;
@property (strong) GTMOAuth2Authentication *auth;

- (GTMOAuth2Authentication *)configureAuthentication;
- (BOOL)authorizeFromKeychainForName:(GTMOAuth2Authentication *) auth;

#if TARGET_OS_IPHONE

- (void)showSignInView:(void (^)(UIViewController *authWindow))presentAuthWindow
           completed:(void (^)(BOOL success))completed;
#else

@property (strong) GTMOAuth2WindowController *authWindow;

- (void)showSignInView:(void (^)(BOOL success))completed;

#endif
@end

@implementation TRAuthController {
#if TARGET_OS_IPHONE
    UIWebView *_webView;
    id<UIWebViewDelegate> _originalDelegate;
#endif
    NSObject *_originalPolicyDelegate;
}

@synthesize auth = _auth;
@synthesize authenticationReady = _authenticationReady;

static TRAuthController *sharedAuthControllerSingleton;

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedAuthControllerSingleton = [[TRAuthController alloc] init];
    }
}

+ (TRAuthController *) sharedInstance
{
    return sharedAuthControllerSingleton;
}

- (id)init {
    self = [super init];
    if (self) {
        _authenticationReady = NO;
        _auth = nil;
        
        
#if TARGET_OS_IPHONE
        TRAppSettings *settings = [TRAppSettings sharedInstance];
        if(settings.reauthenticate) {
            [self signOut];
        }
#endif
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(troupeUrlChanged:)
                                                     name:TroupeUrlChanged
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(troupeSignoutRequested:)
                                                     name:TroupeSignOutRequested
                                                   object:nil];
    }
    return self;
}


- (GTMOAuth2Authentication*) authenticator {
    return _auth;
}

- (GTMOAuth2Authentication*) preAuthenticatedAuthenticator {
    qlinfo(@"Authenticating");
    
    if(_authenticationReady) {
        return _auth;
    }
    
    if(_auth == nil) {
        _authenticationReady = NO;
        _auth = [self configureAuthentication];
    }
    
    if([self authorizeFromKeychainForName:_auth]) {
        _authenticationReady = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:TroupeAuthenticationReady object:self];
        return _auth;
    }

    return nil;
}

- (void) troupeUrlChanged:(NSNotification *)notification {
    qlinfo(@"Authentication signing out");
    [self signOut];
}

- (void) troupeSignoutRequested:(NSNotification *)notification {
    qlinfo(@"Authentication signing out");
    [self signOut];
}

#if TARGET_OS_IPHONE
- (void)authenticate:(void (^)(UIViewController *authWindow))presentAuthWindow
           completed:(void (^)(BOOL success))completed {
#else
- (void)authenticate:(void (^)(BOOL success))completed {
#endif

    qlinfo(@"Authenticating");
    if(_auth == nil) {
        _auth = [self configureAuthentication];
        _authenticationReady = NO;
    }
    
    if([self authorizeFromKeychainForName:_auth]) {
        _authenticationReady = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:TroupeAuthenticationReady object:self];
        
        if(completed) {
            completed(YES);
        }
        
        return;
    }
    
#if TARGET_OS_IPHONE
    [self showSignInView:presentAuthWindow completed:completed];
#else
    [self showSignInView:completed];
#endif
}

#if TARGET_OS_IPHONE
-  (void)intercept:(id)presentView {
    GTMOAuth2ViewControllerTouch *t = (GTMOAuth2ViewControllerTouch *) presentView;
    UIWebView *webView = t.webView;
    
    _webView = webView;
    _originalDelegate = webView.delegate;
    t.webView.scrollView.bounces = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    t.webView.delegate = self;
}

-  (void)endIntercept {
    if(_originalDelegate) {
        _webView.delegate = _originalDelegate;
        _originalDelegate = nil;
        _webView = nil;
    }
}
#else
#endif

#if TARGET_OS_IPHONE
- (void)showSignInView:(void (^)(UIViewController *authWindow))presentAuthWindow
             completed:(void (^)(BOOL success))completed {
#else
 - (void)showSignInView:(void (^)(BOOL success))completed {
#endif
    
    qlinfo(@"Loading sign-in view");
    TRAppSettings *settings = [TRAppSettings sharedInstance];
    
    NSString *keychainItemName = settings.keychainItemName;
    
    NSURL *authURL = settings.oauthAuthorizationURL;
    
     if(!cacheCleared) {
         cacheCleared = YES;
         [self clearBrowserCookies];
     }
    
#if TARGET_OS_IPHONE
    GTMOAuth2ViewControllerTouch *viewController;
    viewController = [GTMOAuth2ViewControllerTouch controllerWithAuthentication:_auth
       authorizationURL:authURL
       keychainItemName:keychainItemName
      completionHandler:^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error) {

          if (error != nil) {
              // Authentication failed (perhaps the user denied access, or closed the
              // window before granting access)
              
              qlerror(@"Authentication completed with error: %@", error);
              NSData *responseData = [[error userInfo] objectForKey:@"data"]; // kGTMHTTPFetcherStatusDataKey
              if ([responseData length] > 0) {
                  // show the body of the server's authentication failure response
                  NSString *str = [[NSString alloc] initWithData:responseData
                                                        encoding:NSUTF8StringEncoding];
                  qlerror(@"Error description %@", str);
              }
              _auth = nil;
              _authenticationReady = NO;
              completed(NO);
              return;
          }
          
          qlinfo(@"Authentication completed successfully");
          _authenticationReady = YES;
          completed(YES);

          [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

          [[NSNotificationCenter defaultCenter] postNotificationName:TroupeAuthenticationReady object:self];
      }];
    
    //viewController.initialHTMLString = @"<html><body>Contacting Troupe server</body></html>";
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    viewController.initialActivityIndicator = activityView;

    viewController.browserCookiesURL = settings.baseServerURL;
    presentAuthWindow(viewController);
    
#else
    [NSApp activateIgnoringOtherApps:YES];

    if(self.authWindow) {
        [self.authWindow.window setOrderedIndex:0];
        if(completed) {
            completed(NO);
        }
        return;
    }
     
    
    self.authWindow = [GTMOAuth2WindowController controllerWithAuthentication:_auth
                                                              authorizationURL:authURL
                                                              keychainItemName:keychainItemName
                                                                resourceBundle:nil];
     
    // optional: display some html briefly before the sign-in page loads
    NSString *html = @"<html><body><div align=center>Loading sign-in page...</div></body></html>";
    [self.authWindow setInitialHTMLString:html];
    self.authWindow.shouldPersistUser = YES;
    
    [self.authWindow signInSheetModalForWindow:nil
                              completionHandler:^(GTMOAuth2Authentication *auth, NSError *error) {
                                  
                                  self.authWindow.webView.policyDelegate = _originalPolicyDelegate;
                                  _originalPolicyDelegate = nil;

                                  if (error != nil) {
                                      // Authentication failed (perhaps the user denied access, or closed the
                                      // window before granting access)
                                      
                                      qlerror(@"Authentication completed with error: %@", error);
                                      NSData *responseData = [[error userInfo] objectForKey:@"data"]; // kGTMHTTPFetcherStatusDataKey
                                      if ([responseData length] > 0) {
                                          // show the body of the server's authentication failure response
                                          NSString *str = [[NSString alloc] initWithData:responseData
                                                                                encoding:NSUTF8StringEncoding];
                                          qlerror(@"Error description %@", str);
                                      }
                                      _auth = nil;
                                      _authenticationReady = NO;
                                      self.authWindow = nil;
                                      if(completed) {
                                          completed(NO);
                                      }
                                      return;
                                          
                                  }
                                  
                                  qlinfo(@"Authentication completed successfully");
                                  _authenticationReady = YES;
                                  self.authWindow = nil;

                                  if(completed) {
                                      completed(YES);
                                  }
                                  
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:TroupeAuthenticationReady object:self];
                                  
                                  /* This only gets calls when the auth window has popped up (not for auto signin) */
                                  [[NSNotificationCenter defaultCenter] postNotificationName:TroupeSignInSuccessful object:self];
                              }];
     _originalPolicyDelegate = self.authWindow.webView.policyDelegate;
    self.authWindow.webView.policyDelegate = self;
     
#endif
}

- (void)clearBrowserCookies {
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in [cookieStorage cookies]) {
        [cookieStorage deleteCookie:cookie];
    }

    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)signOut {
    TRAppSettings *settings = [TRAppSettings sharedInstance];
    NSString *keychainItemName = settings.keychainItemName;
    
    qlinfo(@"Authentication removing keychain auth: %@", keychainItemName);
    
    _auth = nil;
    _authenticationReady = NO;
    
#if TARGET_OS_IPHONE
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:keychainItemName];
#else
    [GTMOAuth2WindowController removeAuthFromKeychainForName:keychainItemName];
#endif
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TroupeSignedOut object:self];
}

- (BOOL)authenticated {
    return _authenticationReady;
}

- (NSString *) authToken {
    if(_auth != nil && _authenticationReady) {
        return _auth.accessToken;
    }

    return nil;
}


- (GTMOAuth2Authentication *)configureAuthentication {
    TRAppSettings *settings = [TRAppSettings sharedInstance];
    
    NSURL *tokenURL = settings.oauthTokenURL;
    
    // We'll make up an arbitrary redirectURI.  The controller will watch for
    // the server to redirect the web view to this URI, but this URI will not be
    // loaded, so it need not be for any actual web page.
    NSString *redirectURI = settings.oauthRedirectURL;

    NSString *clientID = settings.clientID;
    NSString *clientSecret = settings.clientSecret;
    
    NSString* keychainItemName = settings.keychainItemName;
    
    GTMOAuth2Authentication *auth = [GTMOAuth2Authentication authenticationWithServiceProvider:keychainItemName
                                                             tokenURL:tokenURL
                                                          redirectURI:redirectURI
                                                             clientID:clientID
                                                         clientSecret:clientSecret];
    
    auth.scope = settings.oauthScope;
    auth.shouldAuthorizeAllRequests = YES;

    return auth;
}

- (BOOL)authorizeFromKeychainForName:(GTMOAuth2Authentication *) auth {
    TRAppSettings *settings = [TRAppSettings sharedInstance];

    NSString *keychainItemName = settings.keychainItemName;

#if TARGET_OS_IPHONE
    return [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:keychainItemName
                                                       authentication:auth];
#else
    return [GTMOAuth2WindowController authorizeFromKeychainForName:keychainItemName
                                                       authentication:auth];
#endif
}

                 
                 
#pragma mark - WebView Delegate methods
#if TARGET_OS_IPHONE
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    if([request.URL.host isEqualToString:[[[TRAppSettings sharedInstance] baseServerURL] host]]) {
     if([request.URL.path isEqualToString:@"/signup"]) {
         [[UIApplication sharedApplication] openURL:request.URL];
         return NO;
     }
    }

    if([_originalDelegate respondsToSelector:@selector(webView: shouldStartLoadWithRequest: navigationType:)]) {
     return [_originalDelegate webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    }

    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    if([_originalDelegate respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [_originalDelegate webViewDidStartLoad:webView];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if([_originalDelegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [_originalDelegate webViewDidFinishLoad:webView];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if([_originalDelegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [_originalDelegate webView:webView didFailLoadWithError:error];
    }
}
#else 
                 
 /*!
  @method webView:decidePolicyForNavigationAction:request:frame:decisionListener:
  @abstract This method is called to decide what to do with a proposed navigation.
  @param actionInformation Dictionary that describes the action that triggered this navigation.
  @param request The request for the proposed navigation
  @param frame The WebFrame in which the navigation is happening
  @param listener The object to call when the decision is made
  @discussion This method will be called before loading starts, and
  on every redirect.
  */
 - (void)webView:(WebView *)webView
        decidePolicyForNavigationAction:(NSDictionary *)actionInformation
         request:(NSURLRequest *)request
           frame:(WebFrame *)frame
decisionListener:(id<WebPolicyDecisionListener>)listener {
    if(![webView.applicationNameForUserAgent isEqualToString:@"Troupe iPhone/Mobile"]) {
        /* Fool github into giving us a compact view */
        [webView setApplicationNameForUserAgent:@"Troupe iPhone/Mobile"];
    }

    if([request.URL.path isEqualToString:@"/signup"]) {
        [[NSWorkspace sharedWorkspace] openURL:request.URL];
        [listener ignore];
    } else {
        [listener use];
    }
}
 /*!
  @method webView:decidePolicyForNewWindowAction:request:newFrameName:decisionListener:
  @discussion This method is called to decide what to do with an targetted nagivation that would open a new window.
  @param actionInformation Dictionary that describes the action that triggered this navigation.
  @param request The request for the proposed navigation
  @param frame The frame in which the navigation is taking place
  @param listener The object to call when the decision is made
  @discussion This method is provided so that modified clicks on a targetted link which
  opens a new frame can prevent the new window from being opened if they decide to
  do something else, like download or present the new frame in a specialized way.
  
  <p>If this method picks a policy of Use, the new window will be
  opened, and decidePolicyForNavigationAction:request:frame:decisionListner:
  will be called with a WebNavigationType of WebNavigationTypeOther
  in its action. This is to avoid possible confusion about the modifiers.
  */
- (void)webView:(WebView *)webView decidePolicyForNewWindowAction:(NSDictionary *)actionInformation
             request:(NSURLRequest *)request
             newFrameName:(NSString *)frameName
             decisionListener:(id<WebPolicyDecisionListener>)listener {
    [listener use];
}
 
 /*!
  @method webView:decidePolicyForMIMEType:request:frame:
  @discussion Returns the policy for content which has been partially loaded.
  Sent after webView:didStartProvisionalLoadForFrame: is sent on the WebFrameLoadDelegate.
  @param type MIME type for the resource.
  @param request A NSURLRequest for the partially loaded content.
  @param frame The frame which is loading the URL.
  @param listener The object to call when the decision is made
  */
 - (void)webView:(WebView *)webView decidePolicyForMIMEType:(NSString *)type
             request:(NSURLRequest *)request
             frame:(WebFrame *)frame
             decisionListener:(id<WebPolicyDecisionListener>)listener {
    [listener use];
}
 

#endif
                 
                 
                 
@end
