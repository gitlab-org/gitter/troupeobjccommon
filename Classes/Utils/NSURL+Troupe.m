//
//  NSURL+Troupe.m
//  Troupe
//
//  Created by Andrew Newdigate on 22/10/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import "NSURL+Troupe.h"

@implementation NSURL (Troupe)

- (BOOL) matchesExcludingHash:(NSURL *)url2 {
    if([self.absoluteURL isEqual:url2.absoluteURL]) return YES;
    
    NSURL *u1 = [NSURL URLWithString:@"#" relativeToURL:self];
    NSURL *u2 = [NSURL URLWithString:@"#" relativeToURL:url2];
    
    return [u1.absoluteURL isEqual:u2.absoluteURL];
}

- (BOOL) matchesBase:(NSURL *)url2 {
    if([self.absoluteURL isEqual:url2.absoluteURL]) return YES;
    
    NSURL *u1 = [NSURL URLWithString:@"/" relativeToURL:self];
    NSURL *u2 = [NSURL URLWithString:@"/" relativeToURL:url2];
    
    return [u1.absoluteURL isEqual:u2.absoluteURL];
}

@end
