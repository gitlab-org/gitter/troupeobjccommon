//
//  NSURL+Troupe.h
//  Troupe
//
//  Created by Andrew Newdigate on 22/10/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Troupe)

- (BOOL) matchesExcludingHash:(NSURL *)url2;
- (BOOL) matchesBase:(NSURL *)url2;

@end
