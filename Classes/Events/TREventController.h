//
//  AuthController.h
//  OAuth2SampleTouch
//
//  Created by Andrew Newdigate on 11/11/2012.
//
//

#import <Foundation/Foundation.h>
#import "FayeClient.h"

//
// Event Delegate
//
@protocol TREventControllerDelegate <NSObject>

- (void) messageReceived:(NSDictionary *)messageDict channel:(NSString *)channel;
- (void) snapshotReceived:(id) snapshot channel:(NSString *)channel;
- (void) serverSubscriptionEstablished;
- (void) serverSubscriptionDisconnected;

@end

//
// Events
//
@interface TREventController : NSObject<FayeClientDelegate>

@property (nonatomic, readonly) BOOL subscriptionReady;
@property (strong, nonatomic) id <TREventControllerDelegate> delegate;

- (id)initWithDelegate:(id <TREventControllerDelegate>)delegate;

- (void) disconnect;
- (void) reconnect;


@end
