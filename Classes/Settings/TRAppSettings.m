//
//  TRAppSettings.m
//  Troupe
//
//  Created by Andrew Newdigate on 20/11/2012.
//
//

#import "TRAppSettings.h"
#import "TRNotifications.h"

/* --------------------------------------------------
 * CLIENT_KEY and CLIENT_SECRET
 * -------------------------------------------------- */


/* --------------------------------------------------
 * Secrets File
 * -------------------------------------------------- */
 #ifdef DEBUG
 #define SECRETS_PLIST @"GitterSecrets-Dev"
 #else
 #ifdef BETA
 #define SECRETS_PLIST @"GitterSecrets-Beta"
 #else
 #define SECRETS_PLIST @"GitterSecrets-Prod"
 #endif
 #endif


/* --------------------------------------------------
 * Defaults File
 * -------------------------------------------------- */

#ifdef BETA
#define DEFAULTS_PLIST @"GitterDefaults-Beta"
#else
#define DEFAULTS_PLIST @"GitterDefaults"
#endif

@interface TRAppSettings ()

@property (strong, nonatomic) NSString *currentBaseUrl;
@property (strong, nonatomic) NSUserDefaults *secrets;
@property (strong, nonatomic) NSUserDefaults *defaults;

@end

static TRAppSettings *sharedAppSettingsSingleton;

@implementation TRAppSettings {
    int firstRunPostUpdate;
}

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedAppSettingsSingleton = [[TRAppSettings alloc] init];
    }

    NSLog(@"Pulling secrets from SECRETS_PLIST = %@.plist", SECRETS_PLIST);
}

+ (TRAppSettings *) sharedInstance
{
    return sharedAppSettingsSingleton;
}

- (id)init {
    self = [super init];
    if (self) {
        NSString *troupeSecretsPath = [[NSBundle mainBundle] pathForResource:SECRETS_PLIST ofType:@"plist"];
        if(troupeSecretsPath == nil) {
            NSString *failureReason = [NSString stringWithFormat:@"Gitter secrets file not found in bundle: %@.plist. You probably need to add it to the `Gitter/Supporting Files` in Xcode navigator", SECRETS_PLIST];
            NSException* exception = [NSException
                exceptionWithName:@"FileNotFoundException"
                reason:failureReason
                userInfo:nil];

            NSLog(@"%@", failureReason);

            [exception raise];
        }
        NSDictionary *troupeSecrets = [NSDictionary dictionaryWithContentsOfFile:troupeSecretsPath];

        self.secrets = [NSUserDefaults standardUserDefaults];
        [self.secrets registerDefaults:troupeSecrets];

        NSString *troupeDefaultsPath = [[NSBundle mainBundle] pathForResource:DEFAULTS_PLIST ofType:@"plist"];
        NSDictionary *troupeDefaults = [NSDictionary dictionaryWithContentsOfFile:troupeDefaultsPath];

        firstRunPostUpdate = -1;
        self.defaults = [NSUserDefaults standardUserDefaults];
        [self.defaults registerDefaults:troupeDefaults];

        NSString *currentUrl = self.baseServerURLString;
        assert(currentUrl != nil);

        self.currentBaseUrl = currentUrl;

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(settingsChanged:)
                                                     name:NSUserDefaultsDidChangeNotification
                                                   object:nil];

    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) settingsChanged:(NSNotification*)notification {
    NSString *newBaseUrl = self.baseServerURLString;

    BOOL reauthenticate = self.reauthenticate;
    if(reauthenticate) {
        [[NSNotificationCenter defaultCenter] postNotificationName:TroupeSignOutRequested object:self];
    }

    if(![newBaseUrl isEqualToString:self.currentBaseUrl]) {
        qlinfo(@"Troupe url has changed to %@", newBaseUrl);

        self.currentBaseUrl = newBaseUrl;
        [[NSNotificationCenter defaultCenter] postNotificationName:TroupeUrlChanged object:self];
    }
}

- (BOOL) reauthenticate {
    BOOL reauthenticate = [self.defaults boolForKey:@"Reauthenticate"];
    if(reauthenticate) {
        [self.defaults setBool:NO forKey:@"Reauthenticate"];
    }
    return reauthenticate;
}

- (BOOL) debuggingChanged {
    BOOL debuggingPrev = [self.defaults boolForKey:@"DebuggingPrev"];
    BOOL debugging = [self.defaults boolForKey:@"Debugging"];

    if(debugging != debuggingPrev) {
        [self.defaults setBool:debugging forKey:@"DebuggingPrev"];
    }

    return debugging != debuggingPrev;

}


- (BOOL) debugging {
    return [self.defaults boolForKey:@"Debugging"];
}

- (NSURL *) baseServerURL {
    return [NSURL URLWithString: self.baseServerURLString];
}

- (NSString *) baseServerURLString {
    NSString *url = [self.defaults stringForKey:@"TroupeURL"];

    if(url.length < 4 || ![NSURL URLWithString:url]) {
        qlwarning(@"Invalid default TroupeURL");
        [self.defaults removeObjectForKey:@"TroupeURL"];
        [self.defaults synchronize];

        url = [self.defaults stringForKey:@"TroupeURL"];

    }

    if([url hasSuffix:@"/"]) {
        NSRange r;
        r.length = 1;
        r.location = url.length - 1;
        return [url stringByReplacingCharactersInRange:r withString:@""];
    }
    return url;
}

- (NSURL *) baseAPIURL {
    return [NSURL URLWithString: [self.defaults stringForKey:@"TroupeAPIURL"]];
}

- (NSURL *) webSocketURL {
    NSString *url = [self.defaults stringForKey:@"TroupeWSURL"];

    if(url.length < 4 || ![NSURL URLWithString:url]) {
        qlwarning(@"Invalid default TroupeWSURL");

        [self.defaults removeObjectForKey:@"TroupeWSURL"];
        [self.defaults synchronize];

        url = [self.defaults stringForKey:@"TroupeWSURL"];
    }

    return [[NSURL alloc] initWithString:url];
}

- (NSURL *) locationPostURL {
    return [[self baseAPIURL] URLByAppendingPathComponent:@"/v1/location"];
}


- (NSURL *) fromRelativeUrl:(NSString *) relativePath {
    NSString *urlStr = [self.currentBaseUrl stringByAppendingString:relativePath];
    NSURL *url = [NSURL URLWithString:urlStr];

    return url;
}

- (NSURL *) oauthTokenURL {
    NSString* tokenURLString = [self.defaults stringForKey:@"TroupeOAuthTokenURL"];
    tokenURLString = [self.currentBaseUrl stringByAppendingString:tokenURLString];

    NSURL *tokenURL = [NSURL URLWithString:tokenURLString];
    qlinfo(@"Token URL is %@", tokenURLString);
    return tokenURL;
}

- (NSString *) oauthRedirectURL {
    NSString *redirectURI = [self.defaults objectForKey:@"TroupeOAuthRedirectURL"];
    return redirectURI;
}

- (NSURL *) oauthAuthorizationURL {
    NSString *authURLString = [self.defaults stringForKey:@"TroupeOAuthAuthURL"];
    authURLString = [self.currentBaseUrl stringByAppendingString:authURLString];

    qlinfo(@"Authorization URL is %@", authURLString);
    return [NSURL URLWithString:authURLString];
}

- (NSString*) keychainItemName {
    NSString *keychainItemName = [self.defaults objectForKey:@"TroupeOAuthKeychainItemName"];

    keychainItemName = [keychainItemName stringByAppendingString:self.currentBaseUrl];

    return keychainItemName;
}


- (NSString *) clientID {
    return [self.secrets stringForKey:@"OAuthClientId"];
}

- (NSString *) clientSecret {
    return [self.secrets stringForKey:@"OAuthClientSecret"];
}

- (NSString *)oauthScope {
    return [self.secrets stringForKey:@"OAuthCallback"];
}



#if TARGET_OS_IPHONE
- (NSString *) deviceName {
    return [[UIDevice currentDevice] name];
}

- (NSString *) uniqueDeviceId {
    NSUUID *uuid = [[UIDevice currentDevice] identifierForVendor];
    return [uuid UUIDString];
}
#endif

#if TARGET_OS_IPHONE
- (BOOL) locationAccessRequested {
    return [self.defaults boolForKey:@"TroupeLocationAccessRequested"];
}

- (BOOL) notificationAccessRequested {
    return [self.defaults boolForKey:@"TroupeNotificationAccessRequested"];
}

- (void) setLocationAccessRequested {
    [self.defaults setBool:YES forKey:@"TroupeLocationAccessRequested"];
}

- (void) setNotificationAccessRequested {
    [self.defaults setBool:YES forKey:@"TroupeNotificationAccessRequested"];
}

- (BOOL) locationAccessGranted {
    return [self.defaults boolForKey:@"TroupeLocationAccessGranted"];
}

- (BOOL) notificationAccessGranted {
    return [self.defaults boolForKey:@"TroupeNotificationAccessGranted"];
}

- (void) setNotificationAccessGranted {
    [self.defaults setBool:YES forKey:@"TroupeNotificationAccessGranted"];
    [[NSNotificationCenter defaultCenter] postNotificationName:TroupeNotificationAccessGranted object:self];

}

- (void) setLocationAccessGranted {
    [self.defaults setBool:YES forKey:@"TroupeLocationAccessGranted"];
    [[NSNotificationCenter defaultCenter] postNotificationName:TroupeLocationAccessGranted object:self];
}

- (NSString *) appVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (NSString *) appName {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}


- (BOOL)firstRunPostUpdate {
    if(firstRunPostUpdate == -1) {
        NSString *lastStarted = [self.defaults stringForKey:@"LastStartedVersion"];
        NSString *version = [self appVersion];

        if(!lastStarted || ![version isEqualToString:lastStarted]) {
            [self.defaults setObject:version forKey:@"LastStartedVersion"];
            firstRunPostUpdate = 1;
        } else {
            firstRunPostUpdate = 0;
        }

    }

    return firstRunPostUpdate == 1;
}

#endif

@end
