# troupeobjccommons

This is a shared sub-module used for the [iOS mobile app](https://gitlab.com/gitlab-org/gitter/gitter-ios-app) and [old macOS desktop app](https://bitbucket.org/troupe/troupenotifierosx).
